import {Button, Flex, Form, Input, InputNumber, Modal, Select, Tag, Typography} from "antd";
import DayScheduleIndependent from "../docschedule/dayscheduleindependent";

const {Text} = Typography;

const data = [
    {status: "main_modal", text: "Томография носоглотки"},
    {status: "free", text: "Свободно"},
    {status: "nonwork", text: "Нерабочий период"},
    {status: "additional_modal", text: "Ирригоскопия"},
    {status: "main_modal", text: "Томография носоглотки"},
    {status: "free", text: "Свободно"},
    {status: "nonwork", text: "Нерабочий период"}
];

const SlotModal = (props) => {
    const {isModalOpen, setIsModalOpen} = props;

    return (
        <Modal title={"Слоты на день"} open={isModalOpen} onCancel={() => setIsModalOpen(false)}
               onOk={() => setIsModalOpen(false)}
            // width={"100%"}
               cancelButtonProps={{style: {display: 'none'}}}
               okText={"OK"}
        >
            <Flex vertical gap={"middle"}>
                <DayScheduleIndependent data={data}/>
            </Flex>
        </Modal>
    );
}

export default SlotModal;