import {Button, Flex, Spin, Typography} from "antd";
import {apiURL} from "../../config";
import { LoadingOutlined } from '@ant-design/icons';
import {useUser} from "../../hooks/api";
import {useLocation, useNavigate} from "react-router-dom";

const {Title} = Typography;

const MainPage = () => {
    const [user, isLoading, error] = useUser();
    const navigate = useNavigate();


    if (isLoading) {
        return (
            <Flex vertical style={{height: '80vh', width: '100%'}} align={"center"} justify={"center"}>
                <Spin/>
            </Flex>
        );
    } else if (user && user.isAnonymous) {
        return (
            <Flex vertical align={"center"} justify={"center"} gap={"middle"}
                  style={{height: "70vh", width: "100%"}}>
                <Title level={1}>Авторизация</Title>
                <Button type={"dashed"} href={apiURL + "/login"} size={"large"}>
                    Войти
                </Button>
            </Flex>
        );
    } else if (user && !user.isAnonymous) {
        if (user.actorType === 'head') {
            navigate('/head/panel');
        } else if (user.actorType === 'hr') {
            navigate('/hr/manage');
        } else if (user.actorType === 'doc') {
            navigate('/doc/schedule');
        }

    }
}

export default MainPage;