import {useEffect, useState} from "react";
import axios from "axios";
import {apiURL} from "../config";


export const useApi = (url, request_data = {}, options = "") => {
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);


    useEffect(() => {
        setIsLoading(true);

        axios
            .get(apiURL + url + options, {data: request_data})
            .then((res) => {
                setData(res.data);
                setIsLoading(false);
            })
            .catch(err => {
                setError(err.message);
                setIsLoading(false);
            });
    }, [options]);

    return [data, isLoading, error];
}

export const useUser = () => useApi("/user");
export const useUsers = () => useApi("/users");
export const useTags = () => useApi("/tags");

export const postApi = (url, data, setData = ((e) => {}), setError = ((e) => {}),
                        reload = false) => {
    axios
        .post(apiURL + url, data)
        .then((res) => {
            console.log(res.data);
            setData(res.data);

            return res.data;
        })
        .catch(err => {
            console.log(err.message);
            setError(err.message);

            return err.message;
        });

}
export const postRecalcSchedule = (setData, setError) => postApi(
    "/schedule/recalc",
    {},
    setData,
    setError
);

export const postAddHoliday = (start, end) => postApi(
    "/doc/add_holiday",
    {start: start, end: end}
)