# Умная онлайн-платформа для Центра Диагностики и Телемедицины
В этом репозитории расположен исходный код онлайн-платформы для ЦДиТ,
написанной в рамках задания №9 хакатона Лидеры Цифровой Трансформации.

Ниже вы найдете инструкцию по локальному развертыванию.

Powered with React, Flask, nginx, scikit, docker, ...

Стенд: https://health.urlc.pro
## Подготовка
### 1. Склонируйте репозиторий
```bash
git clone git@gitlab.com:tblackcat/nghealth.git
cd nghealth
```
### 2. Заполните .env файл по примеру
```bash
cp example.env .env
vim .env
```

### 3. (Если вы развертываете под доменом) Поменяйте адрес в frontend/src/config.js на адрес домена
```bash
vim frontend/src/config.js
```

## Запуск для разработки
```bash
sudo docker-compose -f docker-compose-hot.yml up --build
```

## Запуск в прод
```bash
sudo docker-compose up --build -d
```
