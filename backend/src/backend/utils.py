import re
import requests
import json

from yandexid import YandexID, YandexOAuth
from flask import redirect, jsonify

from .config import YandexOAuthSettings, BackendSettings
from .algorithms import surveys, create_timetable
from .models import DocActor

ya_settings = YandexOAuthSettings()
backend_settings = BackendSettings()


def construct_redirect(url: str, m_type: str = "", text: str = ""):
    if m_type == "":
        return redirect(url)
    return redirect(f"{url}?{m_type}&text={text}")


def get_auth_url(callback_endpoint: str = "", **additional_arguments) -> str:
    args = "&".join([f"{arg_key}={additional_arguments[arg_key]}" for arg_key in additional_arguments])
    yandex_oauth = YandexOAuth(
        client_id=ya_settings.ya_client_id,
        client_secret=ya_settings.ya_client_secret,
        redirect_uri=ya_settings.ya_redirect_uri + callback_endpoint,
    )

    return yandex_oauth.get_authorization_url(force_confirm=True) + "&" + args


def get_userinfo_from_code(code: str):
    yandex_oauth = YandexOAuth(
        client_id=ya_settings.ya_client_id,
        client_secret=ya_settings.ya_client_secret,
        redirect_uri=ya_settings.ya_redirect_uri
    )

    token = yandex_oauth.get_token_from_code(code)
    yandex_id = YandexID(token.access_token)
    user = json.loads(yandex_id.get_user_info(format="json"))
    return user


def get_predictions():
    return json.loads(requests.get('http://doctors_models:5000/models/predict?start=2024-02-01&end=2024-03-06').json())


def gen_doctors():
    return [doc_actor.get_data() for doc_actor in DocActor.select()]


def get_timetable():
    time_on_surveys = {survey: survey_time for survey, survey_time in zip(surveys, [0.05, 0.3, 0.5, 0.7, 0.1,
                                                                                    0.4, 0.5, 0.8, 0.1, 0.02])}
    predictions = get_predictions()

    return create_timetable(gen_doctors(), predictions, time_on_surveys)


def fill_recalculated_data(timetable, distributed_surveys):
    for doctor in DocActor.select():
        doctor.schedule = {
            "timetable": timetable[doctor.id],
            "surveys": distributed_surveys[doctor.id]
        }
        doctor.save()
