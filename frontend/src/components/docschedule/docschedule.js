import DaySchedule from "./dayschedule";
import {Button, Flex, Spin, Typography} from "antd";
import {CiWavePulse1} from "react-icons/ci";
import Schedule from "./schedule";
import HeadPanelModalRoute from "../headpanel/headpanelmodalroute";
import DocModalRoute from "./docmodalroute";
import {useState} from "react";
import {useUser} from "../../hooks/api";

const {Title} = Typography;

const data = [
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
];


const DocSchedule = () => {
    const [user, isLoadingUser, errorUser] = useUser();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalType, setModalType] = useState(null);

    console.log("user: ", user);
    if (!user) {
        return (
            <Flex vertical align={"center"} justify={"center"} style={{height: '98vh', width: '100%'}}>
                <Spin/>
            </Flex>
        );
    } else {
        return (
            <Flex vertical gap={"middle"}>
                <Title level={1}>Личный кабинет</Title>
                <Flex align={"center"} justify={"center"} gap={"large"}>
                    <Button size={"large"} type={"primary"} onClick={() => {
                        setModalType("Взять отпуск");
                        setIsModalOpen(true);
                    }}>Взять отпуск</Button>
                    <Button size={"large"} onClick={() => {
                        setModalType("Взять отпуск");
                        setIsModalOpen(true);
                    }}>Взять больничный</Button>
                </Flex>
                <Title level={1}>Расписание</Title>
                <Schedule data={user.actor.schedule.surveys}/>
                <DocModalRoute modalType={modalType} isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen}/>
            </Flex>
        );
    }
}


export default DocSchedule;
