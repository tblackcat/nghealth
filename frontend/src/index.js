import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './pages/App/App';
import {ConfigProvider, theme} from "antd";
import {BrowserRouter} from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <ConfigProvider theme={{
                "token": {
                    "colorPrimary": "#16b7ff",
                    "colorInfo": "#16b7ff",
                    "fontFamily": "Montserrat",
                },
                // "components": {
                //     "Layout": {
                //         "headerBg": "rgb(152, 205, 253)",
                //         "algorithm": true
                //     }
                // }
                // "algorithm": theme.darkAlgorithm
            }}>
                <App/>
            </ConfigProvider>
        </BrowserRouter>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
