import {Breadcrumb, Button, Col, Flex, Row, Typography} from 'antd';
import {HomeOutlined} from "@ant-design/icons";
import {apiURL} from "../../config";


const {Title} = Typography;


const HeaderContent = () => {

    return (
        <Flex justify={"space-between"} style={{width: '100vw'}} align={"center"}>
            <Title style={{color: "#ffffff"}} level={1}>Health</Title>
            <Button type={"link"} href={apiURL + "/logout"}>Выйти</Button>
        </Flex>
    );
}

export default HeaderContent;
