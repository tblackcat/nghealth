import SlotModal from "./slotmodal";


const HeadPanelModalRoute = (props) => {
    const {modalType} = props;

    if (modalType === "Слоты") {
        return <SlotModal {...props}/>;
    }
}

export default HeadPanelModalRoute;