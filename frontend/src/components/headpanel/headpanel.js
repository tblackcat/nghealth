import {Button, Flex, Popconfirm, Typography} from "antd";
import ChartCard from "../chartcards/chartcard";
import Tabel from "./tabel";
import {useState} from "react";
import HeadPanelModalRoute from "./headpanelmodalroute";
import {useUsers} from "../../hooks/api";
import {tab} from "@testing-library/user-event/dist/tab";
import RecalcConfirmModal from "./recalcconfirmmodal";
import {apiURL} from "../../config";

const {Title} = Typography;

const HeadPanel = () => {
    const [users, isLoadingUsers, errorUsers] = useUsers();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isRecalcModalOpen, setIsRecalcModalOpen] = useState(false);
    const [modalType, setModalType] = useState(null);

    const tabelData = users ? users.map((userd) => ({
        key: userd.id,
        name: {name: userd.name, data: userd},
        rate: userd.actor.fte,
        modality: userd.actor.mainModal.name,
        additionalModalities: userd.actor.additionalModals.map((modal) => (modal.name)),
        ...userd.actor.schedule.timetable
    })) : []

    return (
        <Flex vertical align={"center"} gap={"middle"}>
            <Title level={1}>Статистика</Title>
            <Flex wrap gap={"large"} justify={"center"}>
                <ChartCard chartTitle={"График 1"}/>
                {/*<ChartCard chartTitle={"График 2"}/>*/}
                {/*<ChartCard chartTitle={"График 3"}/>*/}
                {/*<ChartCard chartTitle={"График 4"}/>*/}
            </Flex>
            <Title level={1}>Табель</Title>
            <Flex align={"center"} justify={"center"} gap={"large"}>
                <Button size={"large"} type={"primary"} href={apiURL + "/schedule/download_timetable"}>Выгрузить
                    табель</Button>
                <Popconfirm
                    title="Пересчитать расписание"
                    description="Вы точно хотите пересчитать расписание? Это займет около минуты."
                    okText="Да"
                    cancelText="Отмена"
                    onConfirm={() => setIsRecalcModalOpen(true)}
                >
                    <Button size={"large"} danger>Пересчитать расписание</Button>
                </Popconfirm>
                {/*<Button size={"large"} disabled>Изменить выходные</Button>*/}
            </Flex>
            <Tabel data={tabelData} isLoading={isLoadingUsers} slotOpen={() => {
                setModalType("Слоты");
                setIsModalOpen(true);
            }}/>
            <HeadPanelModalRoute modalType={modalType} isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen}/>
            <RecalcConfirmModal isModalOpen={isRecalcModalOpen} setIsModalOpen={setIsRecalcModalOpen}/>
        </Flex>
    );
}

export default HeadPanel;