import math
from typing import List, Optional, Tuple, Dict, Set

import numpy as np
import pandas as pd


surveys = {
    'Денситометр',
    'КТ',
    'КТ с КУ 1 зона',
    'КТ с КУ 2 и более зон',
    'ММГ',
    'МРТ',
    'МРТ с КУ 1 зона',
    'МРТ с КУ 2 и более зон',
    'РГ',
    'Флюорограф'
}


###############################-utils-####################################


def can_take_survey(survey, doctor_surveys):
    return (survey in doctor_surveys or
            # for КУ 1/2
            survey[:2] in doctor_surveys or
            survey[:3] in doctor_surveys)


def can_take_mask(doctors: pd.DataFrame, survey: str):
    return doctors[f'is_main_{survey}'] | doctors[f'is_extra_{survey}']


def work_time(doctors: pd.DataFrame):
    return doctors['work_hours']


def free_time(doctors: pd.DataFrame):
    return doctors['max_hours'] - work_time(doctors)


###############################-best-shifts-positions-####################################


def best_shifts_positions(black_list: pd.DataFrame, max_doctors_per_day=260) -> pd.DataFrame:
    shifts = pd.DataFrame(np.zeros_like(black_list.values, dtype=bool),
                          index=black_list.index, columns=black_list.columns)
    # random doctors permutation
    doctors = np.random.permutation(shifts.index)
    for doctor in doctors:
        restrictions = black_list.loc[doctor].values
        free_space = shifts.sum(axis=0) < max_doctors_per_day
        shifts.loc[doctor] = best_shifts_positions_for_doctor((restrictions & free_space).values)
    return shifts


# это лучше не читать
def best_shifts_positions_for_doctor(restrictions: List[bool]) -> List[bool]:
    n = len(restrictions)

    # dp[i][week_work][work][month][mask][couple] - максимальное количество рабочих дней к i-му дню:
    # 0 <= week_work <= 4 - количество рабочих дней в неделе
    # work = 0 - день выходной, work = 1 - день рабочий
    # month = 1 - была неделя в 4 рабочих дня, month = 0 - не было
    # mask - маска рабочих дней до этого дня 0 <= mask < 8 [0, 0, 0], [0, 0, 1], ..., [1, 1, 1]
    # couple = 1 - на текущей неделе было 2 выходных, couple = 1 - не было
    masks_for_work = {
        0: {0, 1, 2, 3, 4, 5, 6},
        1: {0, 1, 2, 4, 5}
    }
    prev_work_for_mask = {
        0: 0, 1: 1,
        2: 0, 3: 1,
        4: 0, 5: 1,
        6: 0, 7: 1,
    }
    prev_masks_for_mask = {
        0: {0, 4},
        1: {0, 4},
        2: {1, 5},
        3: {1, 5},
        4: {2, 6},
        5: {2, 6},
        6: {3},
        7: {}
    }

    # k=0 and mask = [1, 0, -]
    double_weekend_mask = {
        0, 2, 4, 6
    }
    dp = np.full((n + 1, 5, 2, 2, 8, 2), float('-inf'))
    updates = np.full(list(dp.shape) + [6], -1)

    def update(ind_to_update, ind_to_take, add):
        ind_to_update = tuple(ind_to_update)
        ind_to_take = tuple(ind_to_take)
        if dp[ind_to_update] == dp[ind_to_take] + add:
            swap_prob = 2 / 3 #(n - ind_to_update[0]) / (n - ind_to_take[0])
            if np.random.choice([False, True], size=1, p=[1 - swap_prob, swap_prob]):
                updates[ind_to_update] = list(ind_to_take)
        if dp[ind_to_update] < dp[ind_to_take] + add:
            dp[ind_to_update] = dp[ind_to_take] + add
            updates[ind_to_update] = list(ind_to_take)

    for work_week in range(5):
        for work in range(2):
            for mask in range(7):
                if work_week - work >= 0:
                    # на предыдущей неделе было два выходных
                    # не было недели в 4 смены
                    dp[0][work_week][work][0][mask][1] = 0

    for i in range(n):
        for work in range(2):
            if restrictions[i] and work == 1:
                # не работаем в нерабочий день
                continue
            week_day = i % 7
            if week_day == 0:
                # нет ограничений действующих внутри недели
                # с предыдущей недели перебираем все возможные значения
                for mask in masks_for_work[work]:
                    prev_work = prev_work_for_mask[mask]
                    for prev_week_work in range(5):  # на предыдущей неделе работаем сколько угодно
                        for prev_mask in prev_masks_for_mask[mask]:
                            for prev_month in range(2):
                                week_work = work
                                # важно на прошлой неделе было два выходных
                                update([i + 1, week_work, work, prev_month, mask, 0],
                                       [i, prev_week_work, prev_work, prev_month, prev_mask, 1], work)
            else:
                for mask in masks_for_work[work]:
                    for week_work in range(5):
                        prev_work = prev_work_for_mask[mask]
                        prev_week_work = week_work - work  # week_work если выходной или (week_work - 1) если рабочий
                        if prev_week_work < 0:
                            # недействительчный случай когда week_work = 0, work = 1
                            continue
                        for prev_mask in prev_masks_for_mask[mask]:
                            if week_work < 4 or work == 0:
                                for prev_month in range(2):
                                    if work == 0 and mask in double_weekend_mask:
                                        # переход couple = 0 -> couple = 1
                                        update([i + 1, week_work, work, prev_month, mask, 1],
                                               [i, prev_week_work, prev_work, prev_month, prev_mask, 0], 0)
                                        update([i + 1, week_work, work, prev_month, mask, 1],
                                               [i, prev_week_work, prev_work, prev_month, prev_mask, 1], 0)
                                    else:
                                        update([i + 1, week_work, work, prev_month, mask, 0],
                                               [i, prev_week_work, prev_work, prev_month, prev_mask, 0], work)
                                        update([i + 1, week_work, work, prev_month, mask, 1],
                                               [i, prev_week_work, prev_work, prev_month, prev_mask, 1], work)
                            else:
                                # переход month = 0 -> month = 1
                                update([i + 1, 4, 1, 1, mask, 0],
                                       [i, 3, prev_work, 0, prev_mask, 0], 1)
                                update([i + 1, 4, 1, 1, mask, 1],
                                       [i, 3, prev_work, 0, prev_mask, 1], 1)

    #mask_minus_1 = np.random.randint(1, size=dp.shape[1:-1], dtype=bool)
    #mask_minus_2 = np.random.randint(1, size=dp.shape[1:-1], dtype=bool)
    #dp[n, :, :, :, :, 1][mask_minus_1] -= 1
    #dp[n, :, :, :, :, 1][mask_minus_2] -= 2

    final = list()
    to_choose_argmax = dp[n, :, :, :, :, 1]
    to_choose_index = np.nonzero(to_choose_argmax ==
                                 to_choose_argmax[np.unravel_index(to_choose_argmax.argmax(), to_choose_argmax.shape)])
    idx = np.random.randint(len(to_choose_index[0]))
    final_index = [item[idx] for item in to_choose_index]
    index = [n] + final_index + [1]
    while len(final) < n:
        final.append(bool(index[2]))
        index = list(updates[tuple(index)])

    return final[::-1]


###############################-manage-hours-within-one-week-####################################


def find_most_week_survey(among: Set, doctors: pd.DataFrame, remainders: Dict[str, int],
                          time_on_survey: Dict[str, float]) -> str:
    max_surveys = dict()
    for survey in among:
        mask = can_take_mask(doctors, survey)
        max_surveys[survey] = free_time(doctors[mask]).sum() / time_on_survey[survey]

    fulfill_percent = {}
    for survey, max_s in max_surveys.items():
        fulfill_percent[survey] = max_s / remainders[survey]
    assert len(fulfill_percent) > 0
    return min(fulfill_percent, key=fulfill_percent.get)


def find_doctor_for_survey(doctors: pd.DataFrame, survey: str, time_on_survey: float, all_surveys: int) \
        -> Tuple[Optional[int], int]:
    # те, кто может провести исследование
    have_survey_mask = can_take_mask(doctors, survey)
    batch_size = min(30, all_surveys)
    while batch_size > 0:
        # те, у кого хватает времени
        time_enough_mask = free_time(doctors) > (time_on_survey * batch_size)

        can_mask = have_survey_mask & time_enough_mask
        if not can_mask.any():
            batch_size //= 2
            continue

        # выкидываем тех у кого исследование не главная модальность, если есть хотя бы одна главная
        main_mask = doctors[f'is_main_{survey}']
        if (main_mask & can_mask).any():
            # все главной модальность
            mask = main_mask & can_mask
        else:
            # все второстепенной модальности
            mask = can_mask

        to_choose_between = doctors[mask]
        # среди них выбираем лучшего по сменам
        shift_duration = (to_choose_between['rate'] / 40) * 12
        percents_shifts = work_time(to_choose_between) / shift_duration
        # середина 1ой смены
        first_shift_priority = (0.0 < percents_shifts) & (percents_shifts < 0.6)
        # середина > 1ой смены
        middle_shift_priority = (0.4 < (percents_shifts % 1)) & ((percents_shifts % 1) < 0.6)
        priority = first_shift_priority | middle_shift_priority

        if priority.any():
            to_choose_between = to_choose_between[priority]

        random_person = np.random.choice(to_choose_between.index)
        return random_person, batch_size
    return None, 0


def get_hours_on_week(doctors: pd.DataFrame,
                      predictions: Dict[str, int],
                      time_on_survey: Dict[str, float]
                      ) -> Tuple[pd.Series, pd.DataFrame, Dict[str, int]]:
    assert set(predictions.keys()) == surveys
    assert set(time_on_survey.keys()) == surveys

    doctors = doctors.copy()
    doctors['work_hours'] = 0.0

    plan = pd.DataFrame(np.zeros((len(doctors), len(surveys))),
                        index=doctors.index, columns=list(surveys), dtype=int)

    remainders = predictions.copy()
    surveys_to_seek = surveys.copy()
    for survey, r in remainders.items():
        if r == 0:
            surveys_to_seek.remove(survey)

    while len(surveys_to_seek) > 0:
        week_survey = find_most_week_survey(surveys_to_seek, doctors, remainders, time_on_survey)
        doctor, surveys_to_do = find_doctor_for_survey(doctors, week_survey,
                                                       time_on_survey[week_survey], remainders[week_survey])
        if doctor is None:
            surveys_to_seek.remove(week_survey)
            continue
        # update work_hours, plan
        spend_time = time_on_survey[week_survey]
        doctors.at[doctor, 'work_hours'] += spend_time * surveys_to_do

        plan.at[doctor, week_survey] += surveys_to_do

        # update remainders
        remainders[week_survey] -= surveys_to_do
        if remainders[week_survey] == 0:
            surveys_to_seek.remove(week_survey)
    return doctors['work_hours'], plan, remainders


###################################-convert-hours-to-shifts-within-one-week####################################

def hours_to_shifts(hours: pd.Series, rates: pd.Series, best_shifts: pd.DataFrame) -> pd.DataFrame:
    float_shifts = hours / (rates * 12 / 40)
    shift_start = (float_shifts % 1 > 0.1) | \
                  (float_shifts > 0) & (float_shifts < 1)
    float_shifts[shift_start] += 1
    rounded_shifts = float_shifts.apply(lambda x: math.floor(x))

    rounded_shifts = rounded_shifts.where(rounded_shifts <= best_shifts.sum(axis=1), best_shifts.sum(axis=1))
    assert (rounded_shifts <= best_shifts.sum(axis=1)).all()

    # randomly set shifts to best_shifts positions
    accepted_indices = pd.Series(index=rounded_shifts.index, data=[[] for _ in range(len(rounded_shifts))])
    for day in best_shifts.columns:
        doctor_this_day = best_shifts[best_shifts[day]].index
        for doctor in doctor_this_day:
            accepted_indices.at[doctor].append(day)

    # select random shifts using numpy random
    values = np.zeros_like(best_shifts.values, dtype=bool)
    real_shifts = pd.DataFrame(values, index=rounded_shifts.index, columns=best_shifts.columns, dtype=bool)
    for doctor, shifts_this_week in rounded_shifts.items():
        days_to_choose = accepted_indices.loc[doctor]
        days_to_take = np.random.choice(days_to_choose, size=shifts_this_week, replace=False)
        for day in days_to_take:
            real_shifts.at[doctor, day] = True
    return real_shifts

###################################-convert-shifts-to-timetable###################################


def shifts_to_timetable(shifts: pd.DataFrame, rates: pd.Series) -> pd.DataFrame:

    idx = pd.MultiIndex.from_product([shifts.index.values,
                                      ['fromTime', 'tillTime', 'brake', 'total']],
                                     names=['doctors', 'times'])
    out = pd.DataFrame(None, index=idx, columns=shifts.columns)
    for doctor in shifts.index:
        rate = rates.loc[doctor]
        work = shifts.loc[doctor].values
        assert len(work) % 7 == 0
        for day, is_work in zip(shifts.columns, work):
            if is_work:
                if rate >= 30:
                    start = np.random.choice([8, 9])
                    end = start + int(12 * rate / 40)
                    out.at[(doctor, 'fromTime'), day] = f'{start}:00'
                    out.at[(doctor, 'tillTime'), day] = f'{end + int(rate == 60)}:{int(rate == 30)}0'
                    out.at[(doctor, 'brake'), day] = '30' if rate == 30 else '60'
                    out.at[(doctor, 'total'), day] = int(12 * rate / 40)
                elif rate >= 10:
                    start = np.random.choice([8, 9, 14])
                    end = start + int(12 * rate / 40)
                    out.at[(doctor, 'fromTime'), day] = f'{start}:00'
                    out.at[(doctor, 'tillTime'), day] = f'{end}:{3 * int(rate == 30)}0'
                    out.at[(doctor, 'brake'), day] = '30'
                    out.at[(doctor, 'total'), day] = int(12 * rate / 40)
                else:
                    start = np.random.choice([8, 9, 14])
                    end = start + 2
                    out.at[(doctor, 'fromTime'), day] = f'{start}:00'
                    out.at[(doctor, 'tillTime'), day] = f'{end}:00'
                    out.at[(doctor, 'brake'), day] = '30'
                    out.at[(doctor, 'total'), day] = 1.5
    return out


###################################-distribute-surveys-over-shifts###################################

def choose_random_day(shifts_this_week: np.array) -> int:
    among_to_choose = np.where(shifts_this_week)
    random_day = np.random.choice(among_to_choose)
    return random_day


def distribute_surveys_over_shifts(month_surveys: List[pd.DataFrame], shifts: pd.DataFrame) -> pd.DataFrame:
    days = shifts.columns.values
    assert len(days) % 7 == 0
    assert len(days) // 7 == len(month_surveys)

    idx = pd.MultiIndex.from_product([shifts.index.values,
                                      list(surveys)],
                                     names=['doctors', 'surveys'])
    out = pd.DataFrame(None, index=idx, columns=days)
    for week, plan in enumerate(month_surveys):
        days_this_week = days[week * 7: (week + 1) * 7]
        shifts_this_week = shifts[days_this_week]
        sum_shifts = shifts_this_week.sum(axis=1)
        # uniformly distribute surveys over shifts per week
        doctor_to_see = sum_shifts > 0

        plan = plan.copy()[doctor_to_see]
        sum_shifts = sum_shifts.copy()[doctor_to_see]

        uniform_part = plan.floordiv(sum_shifts, axis=0)
        for day in days_this_week:
            for doctor in shifts.index:
                for survey in surveys:
                    if shifts.at[doctor, day] and uniform_part.at[doctor, survey] > 0:
                        out.at[(doctor, survey), day] = uniform_part.at[doctor, survey]
        # randomly distribute the rest
        rest = plan - uniform_part * sum_shifts
        for doctor in plan.index:
            for survey in surveys:
                while rest.loc[(doctor, survey)] > 0:
                    random_idx = choose_random_day(shifts_this_week.loc[doctor].values)
                    random_day = days_this_week[random_idx]
                    if type(out[random_day].loc[(doctor, survey)]) != int:
                        out.at[(doctor, survey), random_day] = 0
                    out.at[(doctor, survey), random_day] += 1
                    plan.at[doctor, survey] -= 1
    return out

###################################-main-####################################


def create_timetable(doctors: List[Dict],
                     predictions: Dict,
                     time_on_survey: Dict[str, float],
                     ) \
        -> Tuple[Dict, Dict]:
    """
    :param doctors: List[Dict]
            [{
            'id': 'doc1',
            'main_modal': 'mod',
            'additional_modal': ['mod1', 'mod2'],
            'rate': 0.25,
            'blacklist': [True, ..., True]
            }, ...]
    :param predictions: Dict
    :param time_on_survey:
        dict[survey, time_on_survey]
    :return:
        timetable: {'doc1': {'day1':
                                {'fromTime': , 'tillTime': , 'brake': , 'total': },
                             'day2':
                                None,
                              ...},
                    'doc2': ...
                   }
        surveys: {'doc1': {'day1': [{'modal': 'КТ', 'total': 12}, {'modal': 'МРТ', 'total': 90}], 'day2': ...},
                  'doc2': {...}, ...}

    """
    doctors = pd.DataFrame(doctors).set_index('id')
    doctors['rate'] = (doctors['rate'] * 40).astype('int')
    black_list_values = np.array(doctors['blacklist'].to_list())
    black_list_columns = [f'day{i}' for i in np.arange(black_list_values.shape[1]) + 1]
    black_list = pd.DataFrame(black_list_values, index=doctors.index, columns=black_list_columns)
    doctors = doctors[['main_modal', 'additional_modals', 'rate']]
    doctors.columns = ['main_survey', 'extra_survey', 'rate']
    predictions = pd.DataFrame(predictions)

    assert set(predictions.columns) == surveys
    assert set(time_on_survey.keys()) == surveys
    assert set(doctors.columns) == {
        'main_survey',
        'extra_survey',
        'rate',
    }
    assert (black_list.index == doctors.index).all()
    assert len(black_list.columns) == len(predictions) * 7

    # prepare doctors dataframe
    doctors = doctors.copy()
    # for fast access
    for survey in surveys:
        doctors[f'is_main_{survey}'] = doctors['main_survey'].apply(lambda x: can_take_survey(survey, [x]))
        doctors[f'is_extra_{survey}'] = doctors['extra_survey'].apply(lambda x: can_take_survey(survey, x))

    # predict best shifts
    best_shifts = best_shifts_positions(black_list)

    # main cycle
    # [hours_per_week] -> [shifts_per_week] -> timetable -> distribute_surveys
    all_shifts = pd.DataFrame(columns=[], index=doctors.index)
    surveys_plan = list()
    remainders = []
    for (i, (_, pred)) in enumerate(predictions.iterrows()):
        print(i)
        days = [f'day{d}' for d in np.arange(i * 7 + 1, (i + 1) * 7 + 1)]
        # max hours withing this week
        doctors['max_hours'] = best_shifts[days].sum(axis=1) * doctors['rate'] * 40
        hours, plan, r = get_hours_on_week(doctors, pred, time_on_survey)
        assert set(hours.keys()) == set(doctors.index)
        assert set(plan.columns) == set(surveys)
        assert set(r.keys()) == set(surveys)

        surveys_plan.append(plan)
        remainders.append(r)

        shifts = hours_to_shifts(hours, doctors['rate'], best_shifts[days])
        assert set(shifts.columns) == set(days)
        assert (shifts.index == doctors.index).all()
        if len(all_shifts.columns) == 0:
            all_shifts = shifts
        else:
            all_shifts = pd.concat([all_shifts, shifts], axis=1)

    # convert all_shifts to from-to-brake timetable
    timetable = shifts_to_timetable(all_shifts, doctors['rate'])
    # stack surveys on shifts
    distributed_surveys = distribute_surveys_over_shifts(surveys_plan, all_shifts)

    timetable_dict = {}
    for doc in timetable.index.levels[0]:
        dict_repr = timetable.loc[doc].to_dict()
        for key, value in dict_repr.items():
            if np.isnan(value['total']):
                dict_repr[key] = None
        timetable_dict[doc] = dict_repr

    distributed_surveys_dict = {}
    for doc in distributed_surveys.index.levels[0]:
        doc_dict = {}
        for day in distributed_surveys.columns:
            day_list = []
            for survey in distributed_surveys.index.levels[1]:
                value = distributed_surveys.loc[(doc, survey), day]
                if not np.isnan(value) and value > 0:
                    day_list.append({'modal': survey, 'count': value})
            doc_dict[day] = day_list
        distributed_surveys_dict[doc] = doc_dict

    timetable.to_excel('/files/timetable.xlsx')
    return timetable_dict, distributed_surveys_dict
