import os
import uuid

from flask import Flask, request, redirect, make_response, jsonify, send_file
from flask_login import LoginManager, UserMixin, login_user, logout_user, \
    current_user, login_required
from flask_cors import CORS

from .models import *
from .config import BackendSettings
from .utils import get_auth_url, get_userinfo_from_code, \
    construct_redirect, get_timetable, fill_recalculated_data
from .algorithms import surveys

backend_settings = BackendSettings()

template_dir = os.path.abspath('./templates')
static_dir = os.path.abspath('./static')
app = Flask(__name__, template_folder=template_dir, static_folder=static_dir)
CORS(app)
cors = CORS(app, resource={
    r"/*": {
        "origins": backend_settings.frontend_baseurl,

    }
})
app.secret_key = backend_settings.flask_secret_key

login = LoginManager(app)

FRONT_URL = backend_settings.frontend_baseurl


@login.user_loader
def load_user(user_id):
    return User.get_or_none(id=user_id)


@app.route("/login")
def login():
    if not current_user.is_anonymous:
        return {
            "status": "ok",
            "text": "Вы уже вошли"
        }
    return construct_redirect(get_auth_url("/api/login/callback"))


@app.route("/logout")
@login_required
def logout():
    if current_user.is_anonymous:
        return construct_redirect(
            FRONT_URL + "/",
            "error",
            "Вы уже вышли"
        )

    logout_user()
    return construct_redirect(
        FRONT_URL + "/",
        "error",
        "Вы успешно вышли из аккаунта!"
    )


@app.route("/login/callback")
def login_callback():
    if not current_user.is_anonymous:
        return construct_redirect(
            FRONT_URL + "/",
            "success",
            "Вы уже авторизованы!"
        )

    auth_code = request.args.get("code")
    if auth_code is None:
        return construct_redirect(
            FRONT_URL + "/",
            "error",
            "Ошибка авторизации"
        )

    try:
        user_info = get_userinfo_from_code(auth_code)
    except Exception as e:
        return construct_redirect(
            FRONT_URL + "/",
            "error",
            str(e)
        )

    user = User.get_or_none(email=user_info["default_email"])
    if user is None:
        return construct_redirect(
            FRONT_URL + "/",
            "error",
            "Пользователь не найден. Обратитесь к вашему HRу"
        )

    login_user(user)

    return construct_redirect(
        FRONT_URL + "/",
        "success",
        "Вы успешно авторизовались!"
    )


@app.route("/user")
def on_get_user():
    if current_user.is_anonymous:
        return {"isAnonymous": True}
    return current_user.get_json()


@app.route("/users")
@login_required
def on_get_users():
    if current_user.actor_type not in ("hr", "head"):
        return {
            "status": "error",
            "text": "No access"
        }, 401

    return [
        user.get_json() for user in User.select().where(User.actor_type == "doc")
    ]


@app.route("/schedule/recalc", methods=['POST'])
@login_required
def on_get_timetable():
    if current_user.actor_type not in ("head", "hr"):
        return {
            "status": "error",
            "text": "No access"
        }, 401

    a, b = get_timetable()
    fill_recalculated_data(a, b)

    return {
        "status": "ok",
        "message": "Successfully recalculated!"
    }


@app.route("/schedule/download_timetable")
@login_required
def on_download_timetable():
    if current_user.actor_type not in ("head", "hr"):
        return {
            "status": "error",
            "text": "No access"
        }, 401

    return send_file(
        "/files/timetable.xlsx",
        as_attachment=True,
        download_name="timetable.xlsx"
    )


@app.route("/tags")
def on_get_tags():
    return list(surveys)


@app.route("/doc/add_holiday", methods=['POST'])
@login_required
def on_add_holiday():
    if current_user.actor_type not in ("doc",):
        return {
            "status": "error",
            "text": "No access"
        }, 401

    data = request.get_json(force=True)
    start = data["start"]
    end = data["end"]

    holiday = Holiday.create(
        doc=current_user.doc_actor,
        start=start,
        end=end,
        motivation=""
    )
    holiday.save()

    return {
        "status": "ok",
        "message": "Successfully added holiday!"
    }