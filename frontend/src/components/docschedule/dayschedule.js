import React, {useContext} from 'react';
import {Table, Badge, Flex, Typography, Skeleton, Card} from 'antd';
import './dayschedule.css';
import {GlobalContext} from "../../pages/App/App";

const {Text} = Typography;

const DaySchedule = (props) => {
    const {data} = props; // data = {date: '01.02', surveys: [{'modal': 'КТ', 'count': 23}, ...]}
    const {tags} = useContext(GlobalContext);

    if (!tags) {
        return (<Skeleton active/>);
    } else {

        const columns = [
            {
                title: data.date,
                dataIndex: 'modal',
                key: 'modal',
                align: 'center',
                render: (survey, record) => {
                    return (
                        <Flex gap={"middle"} align={"center"} justify={"space-between"}>
                            <Text>{survey}</Text>
                            <Badge count={record.count} style={{backgroundColor: '#52c41a'}}
                                   overflowCount={99999}/>
                        </Flex>
                    );
                }
            }
        ];

        const dataSource = data.surveys.map((survey) => {
            return {
                key: survey.modal,
                modal: survey.modal,
                count: survey.count
            }
        });

        // fill data_source with empty slots if dataSource length is less than tags length
        // empties = {} * (tags.length - dataSource.length)
        const empties = Array(tags.length - dataSource.length).fill({}).map(() => {
            return {
                key: 'empty',
                modal: '',
                count: 0
            }
        });

        return (
            <div style={{width: '16%', overflowX: 'auto'}}>
                <Card bordered style={{height: '40vh'}}>
                    <Table
                        columns={columns}
                        dataSource={dataSource}
                        // bordered
                        pagination={{position: ['none', 'none']}}

                    />
                </Card>
            </div>
        );
    }
};

export default DaySchedule;
