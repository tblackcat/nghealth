import {Button, Form, Input, InputNumber, Modal, Select} from "antd";
import Schedule from "../docschedule/schedule";

const data = [
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "free", text: "Свободно"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "additional_modal", text: "Ирригоскопия"},
        {status: "main_modal", text: "Томография носоглотки"},
        {status: "free", text: "Свободно"},
        {status: "nonwork", text: "Нерабочий период"}
    ],
    [
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
        {status: "nonwork", text: "Нерабочий период"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
    [
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
        {status: "nonwork", text: "Нет информации"},
    ],
];


const ShowDocSchedule = (props) => {
    const {isModalOpen, setIsModalOpen, modalData} = props;

    return (
        <Modal title={"Расписание врача"} open={isModalOpen} onCancel={() => setIsModalOpen(false)}
               onOk={() => setIsModalOpen(false)}
               width={"100%"}
               cancelButtonProps={{ style: { display: 'none' } }}
               okText={"OK"}
        >
            <Schedule data={modalData}/>
        </Modal>
    );
}

export default ShowDocSchedule;