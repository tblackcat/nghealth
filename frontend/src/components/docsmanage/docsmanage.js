import {Flex, FloatButton, Modal, Popconfirm, Space, Table, Tag, Typography} from "antd";
import {IoIosAdd} from "react-icons/io";
import {useState} from "react";
import DocsManageModalContent from "./docsmanagemodal";
import {useUser, useUsers} from "../../hooks/api";

const {Title} = Typography;


const DocsManage = (props) => {
    const [user, isLoadingUSer, errorUser] = useUser();
    const [users, isLoadingUsers, errorUsers] = useUsers();
    const {role} = props;
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [modalType, setModalType] = useState(null);
    const [modalData, setModalData] = useState({name: ""});

    const columns = [
        {
            title: 'ФИО',
            dataIndex: 'name',
            key: 'name',
            render: ({name, data}) => <a onClick={() => {
                setModalType("Личное дело врача");
                setModalData(data);
                setIsModalOpen(true);
            }}>{name}</a>,
        },
        {
            title: 'Осн. специализация',
            dataIndex: 'main_spec',
            key: 'main_spec',
        },
        {
            title: 'Доп. специализации',
            key: 'additional_specs',
            dataIndex: 'additional_specs',
            render: (_, {additional_specs}) => (
                <>
                    {additional_specs.map((tag) => {
                        const color = tag.length > 19 ? 'geekblue' : 'green';

                        return (
                            <Tag color={color} key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        );
                    })}
                </>
            ),
        },
        {
            title: 'Действия',
            key: 'actions',
            render: (_, record) => (
                <Space size="middle">
                    <a onClick={() => {
                        setModalType("Расписание врача");
                        setModalData(record.surveys);
                        setIsModalOpen(true);
                    }}>Расписание</a>
                    <a onClick={() => {
                        setModalType("Изменить врача");
                        setIsModalOpen(true);
                    }}>Изменить</a>
                    <Popconfirm
                        title="Удалить врача"
                        description="Вы точно хотите удалить этого врача?"
                        onConfirm={() => console.log('deleted doc')}
                        onCancel={() => {
                        }}
                        okText="Да"
                        cancelText="Нет"
                    >
                        <a style={{color: "#f5426f"}}>Удалить</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];

    const data = users ? users.map((userd) => ({
        key: userd.id,
        name: {name: userd.name, data: userd},
        main_spec: userd.actor.mainModal.name,
        additional_specs: userd.actor.additionalModals.map((modal) => (modal.name)),
        surveys: userd.actor.schedule.surveys
    })) : []


    return (
        <Flex vertical wrap gap={"middle"} justify={"space-around"}>
            <Title level={2}>Врачи</Title>
            <Table isLoading={isLoadingUsers} columns={columns} dataSource={data}/>
            <FloatButton
                shape="square"
                type="primary"
                style={{
                    left: 24,
                    opacity: "80%"
                }}
                icon={<IoIosAdd/>}
                onClick={() => {
                    setModalType("Добавить врача");
                    setIsModalOpen(true);
                }}
            />

            <DocsManageModalContent modalData={modalData}
                                    modalType={modalType}
                                    isModalOpen={isModalOpen}
                                    setIsModalOpen={setIsModalOpen}/>
        </Flex>
    );
}

export default DocsManage;