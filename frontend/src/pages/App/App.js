import {FloatButton, Layout, message, Modal} from "antd";
import {IoClose, IoCloudUploadOutline} from "react-icons/io5";
import HeaderContent from "./../../components/headercontent/headercontent";
import {createContext, useEffect, useState} from "react";
import {Route, Routes, useLocation, useNavigate} from "react-router-dom";
import HeadPanel from "../../components/headpanel/headpanel";
import DocsManage from "../../components/docsmanage/docsmanage";
import DocSchedule from "../../components/docschedule/docschedule";
import MainPage from "../../components/mainpage/mainpage";
import {useTags} from "../../hooks/api";

const {Header, Content, Footer, Sider} = Layout;


const headerStyle = {
    textAlign: 'center',
    // color: '#',
    // height: 64,
    paddingInline: 48,
    // lineHeight: '64px',
    backgroundColor: '#4096ff',
};

const contentStyle = {
    textAlign: 'center',
    minHeight: 120,
    padding: "2vh",
    lineHeight: '120px',
    // color: '#fff',
    overflowY: "scroll",
    overflowX: "hidden"
    // backgroundColor: '#0958d9',
};

const siderStyle = {
    textAlign: 'center',
    // lineHeight: '120px',
    // color: '#fff',
    // backgroundColor: '#1677ff',
};

const footerStyle = {
    textAlign: 'center',
    // color: '#fff',
    // backgroundColor: '#4096ff',
};

const layoutStyle = {
    borderRadius: 8,
    overflow: 'hidden',
    width: 'calc(100%)',
    maxWidth: 'calc(100%)',
    height: '98vh',
}

export const GlobalContext = createContext(null);

const App = () => {
    const [messageApi, contextHolder] = message.useMessage();
    const [tags, isLoading, error] = useTags();

    const show_error = (text) => {
        messageApi.error(text);
    };

    const show_success = (text) => {
        messageApi.success(text);
    };

    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        const queryParameters = new URLSearchParams(window.location.search);

        if (queryParameters.has('error')) {
            show_error(queryParameters.get('text'))
            queryParameters.delete('error')
            queryParameters.delete('text')
        }

        if (queryParameters.has('success')) {
            show_success(queryParameters.get('text'))
            queryParameters.delete('success')
            queryParameters.delete('text')
        }
        navigate(window.location.pathname);
    }, []);


    return (
        <>
            <Layout style={layoutStyle} key={"main_layout"}>
                <GlobalContext.Provider value={{show_error, show_success, tags}}>
                    <Header
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        <HeaderContent/>
                    </Header>
                    <Content style={contentStyle}>
                        <Routes>
                            <Route exact path="/" element={<MainPage/>}/>
                            <Route exact path="/head/panel" element={<HeadPanel/>}/>
                            <Route exact path="/head/manage" element={<DocsManage mode={"head"}/>}/>
                            <Route exact path="/hr/manage" element={<DocsManage mode={"hr"}/>}/>
                            <Route exact path="/doc/schedule" element={<DocSchedule/>}/>
                        </Routes>

                    </Content>
                    {contextHolder}
                </GlobalContext.Provider>
            </Layout>
        </>
    );

}

export default App;
