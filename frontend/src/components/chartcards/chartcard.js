import {Card, Flex} from "antd";
import {BarChart, LineChart} from "@mui/x-charts";


const ChartCard = (props) => {

    return (
        <Flex vertical align={"center"} justify={"center"} gap={"small"}>
            <Card
                title={"План/Прогноз"}
            >
                <BarChart

                    xAxis={[{
                        scaleType: 'band', data: [
                            'Денситометр',
                            'КТ',
                            'КТ/КУ 1',
                            'КТ/КУ 2',
                            'ММГ',
                            'МРТ',
                            'МРТ/КУ 1',
                            'МРТ/КУ 2',
                            'РГ',
                            'Флюорография']
                    }]}
                    series={[
                        {label: "Прогноз", data: [8973, 27455, 2582, 2961, 89697, 9714, 4208, 77, 378615, 18243],},
                        {label: "План", data: [8260, 21129, 2538, 2537, 91071, 8931, 3899, 64, 332056, 17445],},
                    ]}
                    width={1200}
                    height={400}
                />
            </Card>
        </Flex>
    );
}

export default ChartCard;