import {Button, Form, Input, InputNumber, Modal, Select} from "antd";


const spec_options = [
    {
        value: 'Маммография',
        label: 'Маммография',
    },
    {
        value: 'Рентгеноскопия пищевода, желудка',
        label: 'Рентгеноскопия пищевода, желудка',
    },
    {
        value: 'Ирригоскопия',
        label: 'Ирригоскопия'
    },
    {
        value: 'Другое',
        label: 'Другое'
    }
];

const workday_options = [
    {
        value: 'monday',
        label: 'Понедельник'
    },
    {
        value: 'tuesday',
        label: 'Вторник'
    },
    {
        value: 'wednesday',
        label: 'Среда'
    },
    {
        value: 'thursday',
        label: 'Четверг'
    },
    {
        value: 'friday',
        label: 'Пятница'
    },
    {
        value: 'saturday',
        label: 'Суббота'
    }
];


const AddDocForm = (props) => {
    const {isModalOpen, setIsModalOpen} = props;

    return (
        <Modal title={"Добавить врача"} open={isModalOpen} onCancel={() => setIsModalOpen(false)}
               width={"100vh"}
               cancelText={"Отмена"}
               okText={"Добавить"}
               okButtonProps={{
                   autoFocus: true,
                   htmlType: 'submit',
               }}
               modalRender={(dom) => (
                   <Form
                       name="basic"
                       labelCol={{
                           // span: 8,
                       }}
                       wrapperCol={{
                           // span: 16,
                       }}
                       style={{
                           // maxWidth: 600,
                       }}
                       layout={"vertical"}
                       initialValues={{}}
                       onFinish={(e) => console.log(e)}
                       onFinishFailed={(e) => console.log(e)}
                       autoComplete="off"
                   >
                       {dom}
                   </Form>
               )}
        >

            <Form.Item
                label="ФИО"
                name="name"
                rules={[
                    {
                        required: true,
                        message: 'Введите ФИО!',
                    },
                ]}
            >
                <Input/>
            </Form.Item>
            <Form.Item
                label="Почта для авторизации (Yandex)"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Введите Yandex почту!'
                    },
                    {
                        pattern: "^.+@.+\\..+$",
                        message: 'Это не почта!'
                    }
                ]}

            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Ставка"
                name="FTE"
                rules={[
                    {
                        required: true,
                        message: 'Введите ставку!',
                    },
                ]}
            >
                <InputNumber defaultValue={1} step={0.1}/>
            </Form.Item>
            <Form.Item
                label="Рабочие дни"
                name="working_days"
                rules={[
                    {
                        required: true,
                        message: 'Укажите рабочие дни!'
                    },
                ]}
            >
                <Select options={workday_options} mode={"multiple"}/>
            </Form.Item>
            <Form.Item
                label="Основная специализация"
                name="main_spec"
                rules={[
                    {
                        required: true,
                        message: 'Введите основную специализацию!',
                    },
                ]}
            >
                <Select options={spec_options}/>
            </Form.Item>
            <Form.Item
                label="Дополнительные специализации"
                name="additional_specs"
                rules={[
                    {
                        required: false,
                    },
                ]}
            >
                <Select options={spec_options} mode={"multiple"}/>
            </Form.Item>

        </Modal>
    );
}

export default AddDocForm;