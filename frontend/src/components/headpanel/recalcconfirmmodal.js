import {Flex, Modal, Progress} from "antd";
import Schedule from "../docschedule/schedule";
import {useEffect, useState} from "react";
import {postRecalcSchedule} from "../../hooks/api";


const RecalcConfirmModal = (props) => {
    const [okButtonStatus, setOkButtonStatus] = useState(false);
    const [progress, setProgress] = useState(0);
    const [recalcData, setRecalcData] = useState(null);
    const [recalcError, setRecalcError] = useState(null);
    const [progressStatus, setProgressStatus] = useState("active");
    const {isModalOpen, setIsModalOpen} = props;

    useEffect(() => {
        if (isModalOpen && recalcData) {
            setOkButtonStatus(true);
            setProgressStatus("");
            setProgress(100);
        } else if (isModalOpen && recalcError) {
            setOkButtonStatus(true);
            setProgressStatus("exception");
        } else if (isModalOpen) {
            postRecalcSchedule(setRecalcData, setRecalcError);
            setProgress(0); // Reset progress when modal is opened
            setOkButtonStatus(false);
            const interval = setInterval(() => {
                setProgress(prev => {
                    if (recalcError) {
                        clearInterval(interval);
                        setOkButtonStatus(true);
                        return prev;
                    }
                    if (recalcData) {
                        clearInterval(interval);
                        setOkButtonStatus(true);
                        return 100;
                    }
                    if (prev < 90) {
                        return prev + (90 / 100); // Increment progress by 100/60 every second
                    } else {
                        return prev;
                    }
                });
            }, 1000);
            return () => clearInterval(interval); // Cleanup interval on component unmount or modal close
        }
    }, [isModalOpen, recalcData, recalcError]);

    return (
        <Modal title={"Пересчёт расписания"} open={isModalOpen}
               onOk={() => setIsModalOpen(false)}
            // width={"100%"}
               cancelButtonProps={{style: {display: 'none'}}}
               okButtonProps={{disabled: !okButtonStatus}}
               okText={"OK"}
        >
            <Flex vertical>
                <Progress percent={progress.toFixed(0)} status={progressStatus}/>
            </Flex>
        </Modal>
    );
}


export default RecalcConfirmModal;