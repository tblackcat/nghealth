import DaySchedule from "./dayschedule";
import {Divider, Flex, Table, Typography} from "antd";
import {CiCircleChevDown, CiWavePulse1} from "react-icons/ci";

const {Title} = Typography;


const days = {
    day1: '01.02',
    day2: '02.02',
    day3: '03.02',
    day4: '04.02',
    day5: '05.02',
    day6: '06.02',
    day7: '07.02',
    day8: '08.02',
    day9: '09.02',
    day10: '10.02',
    day11: '11.02',
    day12: '12.02',
    day13: '13.02',
    day14: '14.02',
    day15: '15.02',
    day16: '16.02',
    day17: '17.02',
    day18: '18.02',
    day19: '19.02',
    day20: '20.02',
    day21: '21.02',
    day22: '22.02',
    day23: '23.02',
    day24: '24.02',
    day25: '25.02',
    day26: '26.02',
    day27: '27.02',
    day28: '28.02',
    day29: '29.02',
    day30: '01.03',
}

const Schedule = (props) => {
    const {data} = props;
    // data = {day1: [{modal: 'КТ', count: 23}, ...], day2: [], ...}

    const dayScheduleData = Object.keys(days).map((day) => {
        return {
            date: days[day],
            surveys: data[day]
        }
    });
    console.log(dayScheduleData);

    return (
        <Flex wrap gap={"small"} align={"center"} justify={"space-evenly"}>
            {
                dayScheduleData.map((dayData) => (
                    <DaySchedule data={dayData}/>
                ))
            }
        </Flex>
    );
}


export default Schedule;
