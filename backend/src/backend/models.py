import csv

from peewee import (
    ForeignKeyField,
    CharField,
    AutoField,
    IntegerField,
    FloatField,
    BooleanField,
    TextField,
    DateTimeField,
    Model,
)
import tqdm
from playhouse.postgres_ext import PostgresqlExtDatabase, JSONField
import peeweedbevolve
from flask_login import UserMixin

from .config import DBSettings, BackendSettings

db_settings = DBSettings()
backend_settings = BackendSettings()
db = PostgresqlExtDatabase(
    db_settings.postgres_db,
    user=db_settings.postgres_user,
    password=db_settings.postgres_pw,
    host=db_settings.postgres_host,
    port=db_settings.postgres_port
)


class Modal(Model):
    tag = CharField(unique=True, verbose_name="Тэг модальности")
    name = CharField(verbose_name="Полное название модальности")

    def get_json(self):
        return {
            "tag": self.tag,
            "name": self.name
        }

    class Meta:
        database = db
        table_name = "modals"


class DocActor(Model):
    fte = FloatField(verbose_name="Ставка")

    main_modal = ForeignKeyField(Modal, backref="docs", verbose_name="Основная модальность")
    # additional_modal_refs - ref from AddModalRef to DocActor

    schedule = JSONField(verbose_name="Расписание", default={})

    def get_main_modal(self):
        return self.main_modal

    def get_additional_modals(self):
        for additional_modal_ref in self.additional_modal_refs:
            yield additional_modal_ref.modal

    def get_schedule(self):
        return self.schedule

    def get_json(self):
        return {
            "fte": self.fte,
            "mainModal": self.get_main_modal().get_json(),
            "additionalModals": [add_modal.get_json() for add_modal in self.get_additional_modals()],
            "schedule": self.schedule,
            "blacklist": [len(self.holidays.where((Holiday.start <= i) & (Holiday.end >= i))) > 0 for i in range(5 * 7)]
        }

    def get_data(self):
        return {
            "id": self.get_id(),
            "main_modal": self.get_main_modal().name,
            "additional_modals": [additional_modal.name for additional_modal in self.get_additional_modals()],
            "rate": self.fte,
            "blacklist": [len(self.holidays.where((Holiday.start <= i) & (Holiday.end >= i))) > 0 for i in range(5 * 7)]
            # len == 5*7
        }

    class Meta:
        database = db
        table_name = "doc_actors"


class Holiday(Model):
    doc = ForeignKeyField(DocActor, backref="holidays", verbose_name="Доктор")
    start = IntegerField(verbose_name="Начало")
    end = IntegerField(verbose_name="Конец")
    motivation = CharField(verbose_name="Мотивация")

    class Meta:
        database = db
        table_name = "holidays"


class AddModalRef(Model):
    modal = ForeignKeyField(Modal, backref="additional_modal_refs", verbose_name="Модальность")
    doc = ForeignKeyField(DocActor, backref="additional_modal_refs", verbose_name="Доктор")

    class Meta:
        database = db
        table_name = "additional_modal_refs"


class HrActor(Model):

    def get_json(self):
        return {}

    class Meta:
        database = db
        table_name = "hr_actors"


class HeadActor(Model):
    def get_json(self):
        return {}

    class Meta:
        database = db
        table_name = "head_actors"


class User(UserMixin, Model):
    email = CharField(unique=True, verbose_name="Yandex eMail")
    name = CharField(verbose_name="Full name")

    actor_type = CharField(verbose_name="Actor type")

    head_actor = ForeignKeyField(HeadActor, backref="users", verbose_name="Head actor", null=True, default=None)
    hr_actor = ForeignKeyField(HrActor, backref="users", verbose_name="HR actor", null=True, default=None)
    doc_actor = ForeignKeyField(DocActor, backref="users", verbose_name="Doctor actor", null=True, default=None)

    def get_id(self):
        return self.id

    def get_actor(self):
        if self.actor_type == "head":
            return self.head_actor
        if self.actor_type == "hr":
            return self.hr_actor
        if self.actor_type == "doc":
            return self.doc_actor
        return None

    def get_json(self):
        return {
            "id": self.get_id(),
            "email": self.email,
            "name": self.name,
            "actorType": self.actor_type,
            "actor": self.get_actor().get_json()
        }

    def get_data(self):
        return self.get_actor().get_data()

    class Meta:
        database = db
        table_name = "users"


db.evolve(interactive=False)

if __name__ == "__main__":
    surveys = [
        'Денситометрия',
        'КТ',
        'КТ с КУ 1 зона',
        'КТ с КУ 2 и более зон',
        'ММГ',
        'МРТ',
        'МРТ с КУ 1 зона',
        'МРТ с КУ 2 и более зон',
        'РГ',
        'ФЛГ',
        'РХ',
        'ПЭТ/КТ'
    ]

    print("Creating modals")
    for modal in tqdm.tqdm(surveys):
        if Modal.get_or_none(tag=modal) is None:
            Modal.create(tag=modal, name=modal)

    print("Creating initial admin")
    if User.get_or_none(email="agawchik223@yandex.ru") is None:
        User.create(
            email="agawchik223@yandex.ru",
            name="Админов Админ Админович",
            actor_type="hr",
            hr_actor=HrActor.create(),
        )
        print("Created head")
    else:
        print("Already created head")

    print("Creating test users")
    with open("/base/test_users.csv", 'r') as users_table:
        reader = csv.reader(users_table)
        for row in tqdm.tqdm(reader):
            actor_type = row[0]
            if actor_type == "doc":
                userdata = {
                    "name": row[2].strip(),
                    "email": row[1].strip(),
                    "main_modal": row[3].strip(),
                    "additional_modals": list(map(str, row[4].split(';'))) if row[4] != '' else [],
                    "fte": row[5].strip()
                }
                if User.get_or_none(email=userdata["email"]) is None:
                    user = User.create(
                        email=userdata["email"],
                        name=userdata["name"],
                        actor_type=actor_type,
                        doc_actor=DocActor.create(
                            fte=float(userdata["fte"]),
                            main_modal=Modal.get_or_none(tag=userdata["main_modal"]),
                        )
                    )

                    for add_modal in userdata["additional_modals"]:
                        modal = Modal.get_or_none(tag=add_modal.strip())
                        if modal is not None:
                            modal_ref = AddModalRef.create(modal=modal, doc=user.doc_actor)
