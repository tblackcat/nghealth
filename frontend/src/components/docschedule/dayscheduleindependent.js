import DaySchedule from "./dayschedule";
import {Flex, Table, Typography} from "antd";
import {CiWavePulse1} from "react-icons/ci";

const {Title} = Typography;

const timeranges = [
    '8:00 - 9:00',
    '9:00 - 10:00',
    '10:00 - 11:00',
    '11:00 - 12:00',
    '12:00 - 13:00',
    '13:00 - 14:00',
    '14:00 - 15:00',
]

const columns = [
    {
        title: 'Промежуток',
        dataIndex: 'time',
        key: 'time',
        render: (text) => <Flex align={"center"} justify={"center"} style={{height: '4vh'}}>{text}</Flex>
    },
];

const dataSource = timeranges.map((range, index) => ({
    key: index + 1,
    time: range,
}));

const TimeRangeTable = () => (
    <Table bordered columns={columns} dataSource={dataSource} pagination={false}/>
);

const DayScheduleIndependent = (props) => {
    const {data} = props;

    return (
        <Flex gap={"middle"} justify={"center"}>
            <TimeRangeTable/>
            <DaySchedule width={"70%"} scheduleData={{day: {title: 'ПН', index: 'monday'}, data: data}}/>
        </Flex>
    );
}


export default DayScheduleIndependent;
