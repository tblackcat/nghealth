import {Button, Flex, Form, Input, InputNumber, Modal, Select, Spin, Tag, Typography} from "antd";
import Schedule from "../docschedule/schedule";

const {Text} = Typography;

const spec_options = [
    {
        value: 'Маммография',
        label: 'Маммография',
    },
    {
        value: 'Рентгеноскопия пищевода, желудка',
        label: 'Рентгеноскопия пищевода, желудка',
    },
    {
        value: 'Ирригоскопия',
        label: 'Ирригоскопия'
    },
    {
        value: 'Другое',
        label: 'Другое'
    }
];

const workday_options = [
    {
        value: 'monday',
        label: 'Понедельник'
    },
    {
        value: 'tuesday',
        label: 'Вторник'
    },
    {
        value: 'wednesday',
        label: 'Среда'
    },
    {
        value: 'thursday',
        label: 'Четверг'
    },
    {
        value: 'friday',
        label: 'Пятница'
    },
    {
        value: 'saturday',
        label: 'Суббота'
    }
];

const workday_map = {
    monday: "Понедельник",
    tuesday: "Вторник",
    wednesday: "Среда",
    thursday: "Четверг",
    friday: "Пятница",
    saturday: "Суббота",
    sunday: "Воскресенье"
};

const doc_data = {
    key: '1',
    name: 'Кириенко Максим Сергеевич',
    main_spec: 'Томография носоглотки',
    additional_specs: ['Рентгенография органов грудной клетки', 'Фистулография'],
    fte: 0.5,
    work_days: ['monday', 'wednesday', 'friday']
};


const DocFile = (props) => {
    const {isModalOpen, setIsModalOpen, modalData} = props;

    console.log(modalData);
    if (modalData) {
        return (
            <Modal title={"Личное дело врача"} open={isModalOpen} onCancel={() => setIsModalOpen(false)}
                   onOk={() => setIsModalOpen(false)}
                // width={"100%"}
                   cancelButtonProps={{style: {display: 'none'}}}
                   okText={"OK"}
            >
                <Flex vertical gap={"middle"}>
                    <Flex vertical gap={"little"}>
                        <Text strong>ФИО</Text>
                        <Text>{modalData.name}</Text>
                    </Flex>
                    <Flex vertical gap={"little"}>
                        <Text strong>Ставка</Text>
                        <Text>{modalData.actor.fte}</Text>
                    </Flex>
                    <Flex vertical gap={"little"}>
                        <Text strong>Основная модальность</Text>
                        <Flex>
                            <Tag color="success">{modalData.actor.mainModal.tag}</Tag>
                        </Flex>
                    </Flex>
                    <Flex vertical gap={"little"}>
                        <Text strong>Дополнительные специальности</Text>
                        {modalData.actor.additionalModals.map(spec => (
                            <Flex wrap>
                                <Tag key={spec.tag} color="processing">{spec.tag}</Tag>
                            </Flex>
                        ))}
                    </Flex>

                </Flex>
            </Modal>
        );
    } else {
        return <Spin/>;
    }
}

export default DocFile;