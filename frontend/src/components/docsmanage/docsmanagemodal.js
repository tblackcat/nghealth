import AddDocForm from "./adddocform";
import EditDocForm from "./editdocform";
import ShowDocSchedule from "./showdocschedule";
import DocFile from "./docfile";


const DocsManageModalContent = (props) => {
    const {modalType} = props;

    if (modalType === "Добавить врача") {
        return <AddDocForm {...props}/>;
    } else if (modalType === "Изменить врача") {
        return <EditDocForm {...props}/>;
    } else if (modalType === "Расписание врача") {
        return <ShowDocSchedule {...props}/>;
    } else if (modalType === "Личное дело врача") {
        return <DocFile {...props}/>;
    }
}

export default DocsManageModalContent;