FROM python:3.10

RUN mkdir /base
WORKDIR /base

RUN pip3 install yandexid
COPY requirements.txt .
RUN pip3 install -r requirements.txt

ENTRYPOINT flask --app src.backend.endpoints:app run --host 0.0.0.0 --port 5000 --debug
