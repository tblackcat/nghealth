from pydantic_settings import BaseSettings
from pydantic import Field, Extra


class DBSettings(BaseSettings):
    # db name
    postgres_db: str = Field(env="POSTGRES_DB")
    # db user
    postgres_user: str = Field(env="POSTGRES_USER")
    # db password
    postgres_pw: str = Field(env="POSTGRES_PW")
    # db host
    postgres_host: str = Field(env="POSTGRES_HOST")
    # db port
    postgres_port: int = Field(env="POSTGRES_PORT")

    class Config:
        env_file = "wo-docker.env"
        extra = Extra.ignore


class BackendSettings(BaseSettings):
    # frontend baseurl
    frontend_baseurl: str = Field(env="FRONTEND_BASEURL")
    # Flask secret key
    flask_secret_key: str = Field(env="FLASK_SECRET_KEY")

    class Config:
        env_file = "wo-docker.env"
        extra = Extra.ignore


class YandexOAuthSettings(BaseSettings):
    # client_id
    ya_client_id: str = Field(env="YA_CLIENT_ID")
    # client_secret
    ya_client_secret: str = Field(env="YA_CLIENT_SECRET")
    # redirect_uri
    ya_redirect_uri: str = Field(env="YA_REDIRECT_URI")

    class Config:
        env_file = "wo-docker.env"
        extra = Extra.ignore
