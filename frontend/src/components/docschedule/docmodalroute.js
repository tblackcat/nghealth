import TakeHolidaysForm from "./takeholidaysform";


const DocModalRoute = (props) => {
    const {modalType} = props;

    if (modalType === "Взять отпуск") {
        return <TakeHolidaysForm {...props}/>;
    }
}

export default DocModalRoute;