import {Button, Form, Input, InputNumber, Modal, Select, DatePicker} from "antd";
import TextArea from "antd/es/input/TextArea";
import dayjs from "dayjs";
import {postAddHoliday} from "../../hooks/api";


const { RangePicker } = DatePicker;

const TakeHolidaysForm = (props) => {
    const {isModalOpen, setIsModalOpen} = props;

    return (
        <Modal title={"Заявка на нерабочий период"} open={isModalOpen} onCancel={() => setIsModalOpen(false)}
               width={"100vh"}
               cancelText={"Отмена"}
               okText={"Добавить"}
               okButtonProps={{
                   autoFocus: true,
                   htmlType: 'submit',
               }}
               modalRender={(dom) => (
                   <Form
                       name="basic"
                       labelCol={{
                           // span: 8,
                       }}
                       wrapperCol={{
                           // span: 16,
                       }}
                       style={{
                           // maxWidth: 600,
                       }}
                       layout={"vertical"}
                       initialValues={{}}
                       onFinish={(e) => {
                           const start = e.holiday_time[0].diff(dayjs('2024-02-01'), 'day');
                           const end = e.holiday_time[1].diff(dayjs('2024-02-01'), 'day');
                           postAddHoliday(start, end);
                           setIsModalOpen(false);
                       }}
                       onFinishFailed={(e) => console.log(e)}
                       autoComplete="off"
                   >
                       {dom}
                   </Form>
               )}
        >

            <Form.Item
                label="Период"
                name="holiday_time"
                rules={[
                    {
                        required: true,
                        message: 'Укажите период!',
                    },
                ]}
            >
                <RangePicker placeholder={['Начало', 'Конец']} />
            </Form.Item>
            <Form.Item
                label="Мотивация"
                name="motivation"
                rules={[
                    {
                        required: true,
                        message: 'Введите мотивацию!',
                    },
                ]}
            >
                <TextArea placeholder={"Введите мотивацию.."} rows={4} />
            </Form.Item>

        </Modal>
    );
}

export default TakeHolidaysForm;